/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Brain.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/18 17:13:02 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/18 18:24:09 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Brain.hpp"
#include <sstream>

using std::stringstream;

Brain::Brain()
{
	sleeping = false;
}

string
Brain::identify() const
{
	stringstream ss;
	string pointer;

	ss << this;
	pointer = ss.str();

	// Turn pointer address to uppercase
	for (size_t i = 2; i < pointer.length(); i++)
		if (islower(pointer[i]))
			pointer.replace(i, 1, 1, toupper(pointer[i]));

	return (pointer);
}

bool
Brain::isSleeping()
{
	return (sleeping);
}

void
Brain::setSleeping(bool sleeping)
{
	this->sleeping = sleeping;
}
