/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Human.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/18 17:30:36 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/18 18:01:44 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HUMAN_HPP
#define HUMAN_HPP

#include "Brain.hpp"
#include <string>

using std::string;

class Human
{
	private:
		const Brain *brain;
	public:
		Human();
		~Human();
		string identify() const;
		const Brain& getBrain();
};

#endif
