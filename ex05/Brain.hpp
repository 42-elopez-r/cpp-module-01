/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Brain.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/18 17:02:01 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/18 18:02:32 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BRAIN_HPP
#define BRAIN_HPP

#include <string>

using std::string;

class Brain
{
	private:
		bool sleeping;
	public:
		Brain();
		string identify() const;
		bool isSleeping();
		void setSleeping(bool sleeping);
};

#endif
