/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Human.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/18 17:39:04 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/18 18:01:38 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Human.hpp"

Human::Human()
{
	brain = new Brain();
}

Human::~Human()
{
	delete brain;
}

string
Human::identify() const
{
	return (brain->identify());
}

const Brain&
Human::getBrain()
{
	return (*brain);
}
