/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex04.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/16 21:20:14 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/16 21:27:49 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string>
#include <iostream>

using std::string; using std::cout;
using std::endl;

int
main()
{
	string str = "HI THIS IS BRAIN";
	string *pointer;
	string &reference = str;

	pointer = &str;

	cout << *pointer << endl;
	cout << reference << endl;
}
