/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/16 20:16:26 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/16 20:29:49 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ZombieEvent.hpp"
#include <cctype>
#include <string>

using std::string;

int
main(int argc, char *argv[])
{
	ZombieEvent zev;
	int zombies;
	string type;
	Zombie *zombie;

	// Check that a numeric argument is passed
	if (argc != 2 || !isdigit(argv[1][0]))
		return (1);

	// Generate as many zombies as indicated
	zombies = atoi(argv[1]);
	for (int i = 0; i < zombies; i++)
	{
		switch (i % 4)
		{
			case 0:
				type = "gardener";
				break;
			case 1:
				type = "cook";
				break;
			case 2:
				type = "doctor";
				break;
			case 3:
				type = "unemployed";
			default:
				type = "impossible";
		}

		zev.setZombieType(type);
		zombie = zev.randomChump();
		delete zombie;
	}
	return (0);
}
