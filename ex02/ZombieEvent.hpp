/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieEvent.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/16 19:48:02 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/16 19:55:23 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIE_EVENT_HPP
#define ZOMBIE_EVENT_HPP

#include "Zombie.hpp"
#include <string>

using std::string;

class ZombieEvent
{
	private:
		string type;
	public:
		ZombieEvent();
		void setZombieType(string type);
		Zombie *newZombie(string name);
		Zombie *randomChump();
};

#endif
