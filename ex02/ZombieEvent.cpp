/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieEvent.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/16 19:55:41 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/16 20:29:35 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ZombieEvent.hpp"
#include <iostream>
#include <cstdlib>
#include <ctime>

using std::cout; using std::endl;

ZombieEvent::ZombieEvent()
{
	type = "generic";
	srand(time(NULL));
}

void
ZombieEvent::setZombieType(string type)
{
	this->type = type;
}

Zombie *
ZombieEvent::newZombie(string name)
{
	return (new Zombie(name, type));
}

Zombie *
ZombieEvent::randomChump()
{
	string name;
	Zombie *zombie;

	name.clear();
	for (int i = 0; i < 10; i++)
	{
		if (i % 2)
			name += "qwrtypsdfghjklzxcvbnm"[rand() % 21];
		else
			name += "aeiou"[rand() % 5];
	}

	zombie = newZombie(name);
	zombie->announce();
	return (zombie);
}
