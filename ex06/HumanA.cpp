/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanA.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/18 21:03:04 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/18 21:38:27 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "HumanA.hpp"
#include <iostream>

using std::cout; using std::endl;

HumanA::HumanA(string name, Weapon& weapon)
{
	this->weapon = &weapon;
	this->name = name;
}

void
HumanA::attack() const
{
	cout << name << " attacks with his " << weapon->getType() << endl;
}
