/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanA.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/18 20:52:39 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/18 21:38:06 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HUMANA_HPP
#define HUMANA_HPP

#include "Weapon.hpp"
#include <string>

using std::string;

class HumanA
{
	private:
		Weapon *weapon;
		string name;
	public:
		HumanA(string name, Weapon& weapon);
		void attack() const;
};

#endif
