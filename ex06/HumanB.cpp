/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanB.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/18 21:03:04 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/18 21:39:25 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "HumanB.hpp"
#include <iostream>

using std::cout; using std::endl;

HumanB::HumanB(string name)
{
	this->name = name;
	this->weapon = NULL;
}

void
HumanB::setWeapon(Weapon& weapon)
{
	this->weapon = &weapon;
}

void
HumanB::attack() const
{
	cout << name << " attacks with his " << weapon->getType() << endl;
}
