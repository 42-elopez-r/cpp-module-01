/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanB.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/18 20:52:39 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/18 21:26:15 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HUMANB_HPP
#define HUMANB_HPP

#include "Weapon.hpp"
#include <string>

using std::string;

class HumanB
{
	private:
		Weapon *weapon;
		string name;
	public:
		HumanB(string name);
		void setWeapon(Weapon& weapon);
		void attack() const;
};

#endif
