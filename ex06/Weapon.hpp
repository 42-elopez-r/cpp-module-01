/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Weapon.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/18 20:07:34 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/18 20:15:36 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WEAPON_HPP
#define WEAPON_HPP

#include <string>

using std::string;

class Weapon
{
	private:
		string type;
	public:
		Weapon(string type);
		const string& getType() const;
		void setType(string type);
};

#endif
