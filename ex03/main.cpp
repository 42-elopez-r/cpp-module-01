/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/16 20:16:26 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/16 21:06:10 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ZombieHorde.hpp"

int
main(int argc, char *argv[])
{
	int zombies;
	ZombieHorde *zhrd;

	// Check that a numeric argument is passed
	if (argc != 2 || !isdigit(argv[1][0]))
		return (1);

	zombies = atoi(argv[1]);
	zhrd = new ZombieHorde(zombies);
	zhrd->announce();
	delete zhrd;
	return (0);
}
