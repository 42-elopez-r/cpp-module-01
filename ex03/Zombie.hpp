/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/15 19:44:49 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/16 21:15:49 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIE_HPP
#define ZOMBIE_HPP

#include <string>

using std::string;

class Zombie
{
	private:
		string name;
		string type;
	public:
		Zombie();
		Zombie(string name, string type);
		void announce();
		void setName(string name);
		void setType(string type);
};

#endif
