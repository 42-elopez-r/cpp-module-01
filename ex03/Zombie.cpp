/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/15 19:54:44 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/18 16:49:28 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"
#include <iostream>

using std::cout; using std::endl;

Zombie::Zombie()
{
	name.clear();
	type.clear();
}

Zombie::Zombie(string name, string type)
{
	this->name = name;
	this->type = type;
}

void
Zombie::announce()
{
	cout << "<" << name << " (" << type << ")> Braiiiiiiinnnssss..." << endl;
}

void
Zombie::setName(string name)
{
	this->name = name;
}

void
Zombie::setType(string type)
{
	this->type = type;
}
