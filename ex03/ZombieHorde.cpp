/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieHorde.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/16 20:46:35 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/16 21:15:15 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ZombieHorde.hpp"

ZombieHorde::ZombieHorde(unsigned int n)
{
	array = new Zombie[n];
	amount = n;
	giveRandomNames();
}

ZombieHorde::~ZombieHorde()
{
	delete[] array;
}

void
ZombieHorde::giveRandomNames()
{
	string name;

	for (unsigned int i = 0; i < amount; i++)
	{
		name.clear();
		for (int c = 0; c < 10; c++)
		{
			if (c % 2)
				name += "qwrtypsdfghjklzxcvbnm"[rand() % 21];
			else
				name += "aeiou"[rand() % 5];
		}

		array[i].setName(name);
		array[i].setType("generic");
	}
}

void
ZombieHorde::announce()
{
	for (unsigned int i = 0; i < amount; i++)
		array[i].announce();
}
