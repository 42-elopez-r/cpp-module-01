/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   replace.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/19 20:47:15 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/19 22:45:19 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <cstring>
#include <fstream>
#include <string>
#include <iostream>

using std::string; using std::cout;
using std::endl; using std::ifstream;
using std::ofstream; using std::ios;

/*
 * This function reads from the specified file and returns a heap allocated
 * string with its content. If there's an error it returns NULL.
 */

static string *
read_file(const char *filename)
{
	string *str;
	ifstream f;
	int c;

	f.open(filename, ios::in);
	if (f.fail())
	{
		cout << "Error: can't open " << filename << endl;
		return (NULL);
	}
	str = new string;

	while ((c = f.get()) != EOF)
		*str += c;
	f.close();

	return (str);
}

/*
 * This function writes str to the specified file (overwriting it).
 */

static void
write_file(const string& filename, const string& str)
{
	ofstream f;

	f.open(filename, ios::out);
	if (f.fail())
	{
		cout << "Error: can't open " << filename << endl;
		return;
	}
	
	f << str;
	f.close();
}

/*
 * This function reads the content of the specified file and replaces
 * every occurence of old_str with new_str writing the result to
 * <filename>.replace.
 */

static void
replace(const char *filename, const char *old_str, const char *new_str)
{
	string *str;
	size_t match;
	string new_filename;

	if (!(str = read_file(filename)))
		return;

	// find() starts on match + strlen(new_str) to avoid trying to replace
	// again over an already replaced area, avoiding getting stuck.
	match = 0;
	while ((match = str->find(old_str, match ? match + strlen(new_str) : 0))
			!= string::npos)
		str->replace(match, strlen(old_str), new_str);

	new_filename = filename;
	new_filename.append(".replace");
	write_file(new_filename, *str);
	delete str;
}

int
main(int argc, char *argv[])
{
	if (argc != 4 || strlen(argv[2]) == 0 || strlen(argv[3]) == 0)
	{
		cout << "Use: " << argv[0] << " FILENAME OLD NEW" << endl;
		return (1);
	}

	replace(argv[1], argv[2], argv[3]);
	return (0);

}
