#!/bin/sh

[ -x ./replace ] || exit 1
rm -f lorem_ipsum.txt.replace stallman_interjection.txt.replace

clear
echo "==> running ./replace lorem_ipsum.txt dolor 'tortilla de patatas con cebolla'"
./replace lorem_ipsum.txt dolor 'tortilla de patatas con cebolla'
diff --color=always lorem_ipsum.txt lorem_ipsum.txt.replace
echo "============================"
echo "Press Enter"
read

clear
echo "==> running ./replace stallman_interjection.txt 'GNU/Linux' 'Microsoft Windows'"
./replace stallman_interjection.txt 'GNU/Linux' 'Microsoft Windows'
diff --color=always stallman_interjection.txt stallman_interjection.txt.replace
echo "============================"
echo "Press Enter"
read

clear
echo "==> running ./replace lorem_ipsum.txt dolor dolor"
./replace lorem_ipsum.txt dolor dolor
diff --color=always lorem_ipsum.txt lorem_ipsum.txt.replace
echo "============================"
echo "Press Enter"
read
