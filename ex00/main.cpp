/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/14 20:49:09 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/14 20:55:39 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Pony.hpp"
#include <iostream>

using std::cout; using std::endl;

static void
ponyOnTheHeap()
{
	Pony *p;

	p = new Pony("Twilight Sparkle", "purple", 25, true);
	p->identify();
	p->feed();
	p->feed();
	delete p;
}

static void
ponyOnTheStack()
{
	Pony p("Fluttershy", "yellow", 27, true);

	p.identify();
	p.feed();
	p.feed();
}

int
main()
{
	cout << " == Heap pony:" << endl;
	ponyOnTheHeap();
	cout << " == Stack pony:" << endl;
	ponyOnTheStack();
	return (0);
}
