/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Pony.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/14 20:14:52 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/14 20:44:33 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PONY_HPP
#define PONY_HPP

#include <string>

using std::string;

class Pony
{
	private:
		string name;
		string color;
		int age;
		bool is_hungry;
	public:
		Pony();
		Pony(string name, string color, int age, bool is_hungry);
		void identify();
		void feed();
};

#endif
