/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Pony.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/14 20:29:11 by elopez-r          #+#    #+#             */
/*   Updated: 2021/05/14 20:56:27 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Pony.hpp"
#include <iostream>

using std::cout; using std::endl;

Pony::Pony()
{
	name = string("Unnamed pony");
	color = string("brown");
	age = 15;
	is_hungry = true;
}

Pony::Pony(string name, string color, int age, bool is_hungry)
{
	this->name = name;
	this->color = color;
	this->age = age;
	this->is_hungry = is_hungry;
}

void
Pony::identify()
{
	cout << "I'm a pony named " << name << ". My color is " << color;
	cout << " and I'm " << age << " years old." << endl;
	if (is_hungry)
		cout << "*Hungry pony noises*" << endl;
}

void
Pony::feed()
{
	if (is_hungry)
	{
		cout << "Ñom ñom *burps*" << endl;
		is_hungry = false;
	}
	else
		cout << "I don't want your filthy food, human" << endl;
}
